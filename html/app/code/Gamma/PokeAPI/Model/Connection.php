<?php


namespace Gamma\PokeAPI\Model;


use Gamma\PokeAPI\Api\ConnectionInterface;
use Magento\Framework\HTTP\Client\CurlFactory;
use Magento\Framework\Serialize\Serializer\Json;

class Connection implements ConnectionInterface
{
    protected $baseUrl;

    /**
     * @var Json
     */
    protected $jsonSerializer;

    /**
     * @var CurlFactory
     */
    protected $httpClient;

    public function __construct(
        Json $jsonSerializer,
        CurlFactory $httpClient,
        string $baseUrl = 'https://pokeapi.co/api'
    )
    {
        $this->baseUrl = $baseUrl;
        $this->jsonSerializer = $jsonSerializer;
        $this->httpClient = $httpClient;
    }

    public function get(string $resourcePath): array
    {
        $requestPath = "{$this->baseUrl}/{$resourcePath}";

        $client = $this->httpClient->create();

        $client->get($requestPath);

        $response = $client->getBody();

        return $this->jsonSerializer->unserialize($response);
    }
}